%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [Qmin, Qmax] = DisplayLevel(Xcat, Ydata, varargin)
%
% This function colors the amount of data Ydata between nested quantiles 
% Quant(1,i) and Quant(2,i) split by category Xcat.
%
%  input: - Xcat: Nx1 category
%         - Ydata: NxM data
%         - varargin: set of option cell like {'opt1', 'val1', ... }
%             Options available:
%                * 'Quantiles': Quantiles list 
%                * 'Color'    : Color of the set
%                * 'Light'    : Opacity max
%                * 'Figure'   : Index of the figure to use
%                * 'Subfigure': Indexes of subplot (eg [1,1,1])
%
% output: - Qmin: min bounds each level
%         - Qmax: max bounds each level
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Qmin, Qmax] = DisplayLevel(Xcat, Ydata, varargin)

% --- Wrong call
if nargin < 2
  printhelp();
  error('wrong number arguments');
end

% --- Parse option
optsnames = {'Quantiles', 'Color', 'Light', 'Figure', 'Subfigure'};
optsvals  = {[0.05:0.15:0.45; 0.95:-0.15:0.55], "b", 0.5, 1, [1,1,1]};

if mod(length(varargin),2)~=0
  warning('wrong number of options: use default opts');
else
  for i = 1:(length(varargin)/2)
    pos = find(strcmpi(optsnames, varargin(2*(i-1)+1)));
    if isempty(pos)==0
      optsvals{pos} = varargin{2*i};
    end
  end
end

[Quant, Color, Light, Fig, Sfig] = optsvals{:};

% --- Get sizes
M = size(Quant,2);
N = length(Xcat);
Qmin = zeros(M,N);
Qmax = zeros(M,N);

% --- display
figure(Fig)
subplot(Sfig(1),Sfig(2),Sfig(3))
hold on
Light = Light/M;

for i=1:M
    [Qmin(i,:), Qmax(i,:)] = DisplaySet(Xcat, Ydata, ...
        'Quantiles', Quant(:,i), 'Color', Color, ...
        'Light', Light, 'Figure', Fig, 'Subfigure', Sfig);
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% Help
function printhelp()
    fprintf('%s\n\n','Invalid call to DisplayLevel. Correct usage is:');
    fprintf('%s\n',' -- [Qmin, Qmax] = DisplayLevel(Xcat, Ydata, varargin)');
    fprintf('%s\n',' -- DisplayLevel(X,Y)');
    fprintf('%s\n',' -- DisplayLevel(X,Y,''figure'', 1)');
    fprintf('%s\n',' -- DisplayLevel(X,Y,''figure'', 1, ''Color'', ''r'', ''light'', 0.3)');
    fprintf('%s\n',' -- DisplayLevel(X,Y,''Quantiles'', [0.1:0.1:0.4; 0.9:-0.1:0.6])');
    fprintf('\n');
    fprintf('''quantiles''  [Qmin; Qmax] Inter-quantiles\n');
    fprintf('''color''      Color of the set\n');
    fprintf('''light''      Opacity max (in [0,1])\n');
    fprintf('''figure''     Figure index where to display\n');
    fprintf('''subfigure''  Subplot indexing\n');
    fprintf('\n');
end